package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{

    @Test
    public void echoCheck()
    {
        assertEquals("check if method echo returns the correct values.", 5 , App.echo(5));
    }

    @Test
     public void bugTest()
     {
        assertEquals("intentional bug", 6 , App.oneMore(5));
     }

}